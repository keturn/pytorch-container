#!/usr/bin/sh
set -e
exec podman run --device nvidia.com/gpu=all --user root --publish 3022:3022 \
  --mount=type=bind,source="${HOME}"/.ssh/id_ed25519.pub,target=/home/ubuntu/.ssh/authorized_keys,readonly=true \
  localhost/python-torch:latest "$@"
