#!/usr/bin/sh
set -e
exec podman run --device nvidia.com/gpu=all --user 1000:1000 --tty --interactive localhost/python-torch:latest "$@"
