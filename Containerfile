# syntax=docker/dockerfile:1

FROM docker.io/phusion/baseimage:noble-1.0.0 AS baseimage-smaller

RUN rm -f /etc/apt/apt.conf.d/docker-clean; echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' > /etc/apt/apt.conf.d/keep-cache

ADD build-scripts/root/apt-get-qy /usr/local/sbin/

ENV DEBIAN_FRONTEND=noninteractive

# drop the gpg-related things frome phusion/baseimage,
# use opensysusers as an alternative to most of the systemd stuff pulled in by cron and logrotate.
# baseimage's my_init script does require python3, so keep that.
RUN --mount=type=cache,target=/var/cache/apt,sharing=locked \
    --mount=type=cache,target=/var/lib/apt/lists,sharing=locked \
    apt-mark auto software-properties-common gpg-agent dirmngr && \
    apt-mark manual python3-minimal && \
    apt-get-qy anacron systemd-sysv- systemd- opensysusers && \
    apt-mark auto opensysusers && \
    apt-get --quiet upgrade --no-install-recommends --autoremove --assume-yes


######################################
FROM baseimage-smaller AS py312

ARG _DEPS="curl ca-certificates git build-essential xz-utils python3.12-venv"

RUN --mount=type=cache,target=/var/cache/apt,sharing=locked \
    --mount=type=cache,target=/var/lib/apt/lists,sharing=locked \
    apt-get-qy ${_DEPS}

WORKDIR /home/ubuntu
USER 1000:1000
RUN mkdir -p ~/.cache/pip

ADD bashrc /home/ubuntu/.bashrc

RUN --mount=type=cache,id=pip-cache,target=/home/ubuntu/.cache/pip,uid=1000,gid=1000 \
    python3.12 -m venv --upgrade-deps /home/ubuntu/venv3.12/

######################################
FROM py312 AS py312-torch24

ENV PATH="/home/ubuntu/venv3.12/bin:/home/ubuntu/.pyenv/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin"
RUN --mount=type=cache,id=pip-cache,target=/home/ubuntu/.cache/pip,uid=1000,gid=1000 \
    pip3 install --use-pep517 'torch~=2.4' --index-url https://download.pytorch.org/whl/cu124


######################################
FROM py312-torch24 as py312-torch24-sshd

RUN mkdir ~/.ssh && chmod go-rwx ~/.ssh

USER 0
WORKDIR /

# - ssh and rsync, for the IDE's "remote interperter" that understands ssh better than podman
ARG _SSH_DEPS="openssh-server openssh-sftp-server rsync"

RUN --mount=type=cache,target=/var/cache/apt,sharing=locked \
    --mount=type=cache,target=/var/lib/apt/lists,sharing=locked \
    apt-get-qy ${_SSH_DEPS}

# enable phusion/baseimage's ssh server
RUN rm -f /etc/service/sshd/down
# unlock the user account so it may log in (with a key)
RUN usermod --password '*' ubuntu

ARG SSH_PORT=3022
RUN echo Port ${SSH_PORT} >> /etc/ssh/sshd_config

EXPOSE $SSH_PORT
