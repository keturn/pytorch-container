#!/usr/bin/sh
set -e
# SELinux hosts may need --security-opt=label=disable
exec podman build \
	--device nvidia.com/gpu=all \
	--tag=python-torch:latest "$@" .
